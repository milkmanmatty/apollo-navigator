# Apollo Navigator

This project was created from scratch for Pendula. It was created in its entirety by Matthew Stubbins.

## Design Decisions

- The project is written in React + TypeScript. A conscious decision was made early on to try and avoid using Redux purely for the fun of it. It has been some time since I have written a pure React (without Redux) program and I wanted to experience how much more difficult it was. This was done in under the pretense of "Have Fun. Don't Panic" and not for any architectural, performance or any other reason.
Having completed the exercise I can say that using Redux would have made the project faster and likely cut down on much of the callback and reference complexity.

- In the interest of reducing time spent the styling was limited and most things that were not explicitly specified in the program brief were styled in the quickest way possible. As such the page itself is not much to look at with components such as the button having their default browser styling.

- The hover tooltip was not specified if it should be shown when hovering on icons that are "unavailable". Likely they would not be shown, however in the intest of reducing time spent no special consideration was given for that case.

- This project was the first time I've used split css file at the component level. This was at the suggestion of a collegue some months ago for me to give it a try. Normally I would use Sass and group smaller component css with parent css. For example I would typically bundle `form-item.css` with its parent component `navigation-section.css`. I enjoyed the separation of concerns it brought and the ease of which finding the right place to put new selectors, however the amount of files with little in them does irk me.

- The codebase went through some changes in how the form was displayed, originally the form was displayed via children elements. This had the benefit of easily allowing all HTML form elements practically out of the box (input, textarea, select etc). However after realising that the form would almost certainly be populated from a CMS or database or similar that approach was scrapped and instead the data-driven approach you currently see was used.

- The icons used were grabbed from [IcoMoo](https://icomoon.io/app) which is a handy app that collates icon and lets you pick the ones you want. It then generates font, svg and css files containing the selected icons.

## Further Improvements

- Testing. All components need their unit tests and I was hoping to get time to setup some more involved Cypress stubbing tests. Sadly this did not come to fruition.

- Small device layout change. Currently on devices below 480px the layout is broken. Ideally the vertical navigation layout would turn horizontal and the tooltips would hide at this breakpoint.

- While this is a "throw-away" exercise there is still a fair few hard-coded elements in the codebase. Two examples are `hardCodedColors` and `hardCodedIcons` in `FormWithNavigation.tsx`. Given time I would rip these out and have them imported in via a data file (similar to `FormData.ts` and `IconTooltips.ts`) and then passed into the component via props.

- Allow more form elements to be displayed in the FormItems.tsx as currently on input elements are shown.

- Use a third party library to handle validation of FormItems.

## Known Issues

- Changing NavigationSections from an already completed section to the most current section will break the automatic "continue" aspect of the "Save And Continue" button.

## Running the project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). As such, in the project directory, you can run:

### `npm install`

Run this after the initial git clone:
`git clone https://milkmanmatty@bitbucket.org/milkmanmatty/apollo-navigator.git`

Once all the node modules have been installed move on to one of the following:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.