/**
 * @jest-environment jsdom
 */
import * as React from "react";
import { render, screen } from "@testing-library/react";
import NavigationSection from "../components/navigation_section/NavigationSection";

describe('Navigation Section component', () => {
	test('renders wrapping div element', () => {
		render(<NavigationSection />);
		const divElement = screen.getByTestId("nav-section");
		expect(divElement).toBeInTheDocument();
	});
});
