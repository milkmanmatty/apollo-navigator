import React from 'react';
import { render, screen } from '@testing-library/react';
import App from '../App';

describe('Root App component', () => {
	test('renders wrapping div element', () => {
		render(<App />);
		const divElement = screen.getByTestId("app");
		expect(divElement).toBeInTheDocument();
	});
});