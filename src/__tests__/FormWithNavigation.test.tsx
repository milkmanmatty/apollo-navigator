/**
 * @jest-environment jsdom
 */
import * as React from "react";
import { render, screen } from "@testing-library/react";
import FormWithNavigation from "../components/form_with_navigation/FormWithNavigation";

describe('Form With Navigation component', () => {
	test('renders wrapping div element', () => {
		render(<FormWithNavigation navigationSections={1} />);
		const divElement = screen.getByTestId("form-with-nav");
		expect(divElement).toBeInTheDocument();
	});

	test('renders correct amount of nav sections', () => {
		render(<FormWithNavigation navigationSections={3} />);
		const divElements = screen.getAllByTestId("nav-section");
		expect(divElements.length).toBe(3);
	});
});
