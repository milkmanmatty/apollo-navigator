import React, { useRef, useState } from "react";
import iconColor from "../../types/iconColor";
import iconPictures from "../../types/iconPictures";
import navStatus from "../../types/navStatus";
import './nav-icon.css'

type stylingOption = {
	holderBG: string,
	iconClass: string,
	iconBG: string,
	iconColor: string,
	titleBG: string,
	titleColor: string,
	titleBorder: string
}

//doesn't need to be merge-able
type navIconProps = {
	colors: iconColor,
	icons: iconPictures,
	iconIndex: number
	iconTitle: string,
	isActive: boolean,
	onIconClick: any,
	parentListCapacity: number,
	status: navStatus
};

const NavigationIcon = (props: navIconProps) => {

	const [hoverClass, setHoverClass] = useState("");
	const timeoutRef = useRef<NodeJS.Timeout>();

	const onMouseLeave = () => {
		timeoutRef.current = setTimeout(() => {
			setHoverClass("");
		}, 205)
	}

	const onMouseEnter = () => {
		if(timeoutRef.current){
			clearTimeout(timeoutRef.current)
		}
		setHoverClass("show");
	}

	const checkActive = (ifNotActive: string) => {
		if (props.isActive) {
			return props.colors.active;
		}
		return ifNotActive;
	}

	const calculateConColor = ():string => {
		if (props.status === "invalid") {
			if (props.isActive) {
				return props.colors.unavailable;
			}
			return props.colors.invalid;
		} else if (props.status === "completed") {
			if (props.isActive) {
				return props.colors.active;
			}
			return props.colors.completed;
		} else {
			return props.colors.unavailable;
		}
	}

	const generateConnector = () => {
		if (props.iconIndex + 1 !== props.parentListCapacity) {
			return (
				<div
					className="nav-icon-connector"
					style={{ backgroundColor: calculateConColor() }}
				></div>
			)
		}
	}

	const calculateColoursAndIcons = ():stylingOption =>{
		if (props.status === "unavailable") {
			return {
				holderBG: "white",
				iconClass: props.icons.unavailable,
				iconBG: props.colors.unavailable,
				iconColor: props.colors.unavailable,
				titleBG: props.colors.unavailable,
				titleColor: "white",
				titleBorder: props.colors.unavailable
			};
		} else if (props.status === "completed") {
			return {
				holderBG: checkActive(props.colors.completed),
				iconClass: props.icons.completed,
				iconBG: checkActive(props.colors.completed),
				iconColor: "white",
				titleBG: checkActive(props.colors.completed),
				titleColor: "white",
				titleBorder: checkActive(props.colors.completed)
			};
		} else if (props.status === "invalid") {
			return {
				holderBG: checkActive(props.colors.invalid),
				iconClass: props.icons.invalid,
				iconBG: checkActive(props.colors.invalid),
				iconColor: "white",
				titleBG: checkActive(props.colors.invalid),
				titleColor: "white",
				titleBorder: checkActive(props.colors.invalid)
			};
		} else {
			return {
				holderBG: checkActive(props.colors.available),
				iconClass: props.icons.available,
				iconBG: "white",
				iconColor: "white",
				titleBG: checkActive(props.colors.available),
				titleColor: "white",
				titleBorder: checkActive(props.colors.available)
			};
		}
	}

	return (
		<div className="nav-icon-wrapper">
			<div
				className={"nav-icon-holder "+props.status+" "+hoverClass}
				onClick={() => props.onIconClick(props.iconIndex)}
				onMouseEnter={onMouseEnter}
				onMouseLeave={onMouseLeave}
				style={{ backgroundColor: calculateColoursAndIcons().holderBG }}
			>
				<i className={calculateColoursAndIcons().iconClass} style={{
					backgroundColor: calculateColoursAndIcons().iconBG,
					color: calculateColoursAndIcons().iconColor
				}}></i>
				<span className="nav-icon-title" style={{
					backgroundColor: calculateColoursAndIcons().titleBG,
					borderColor: calculateColoursAndIcons().titleBorder, //allow inheritance, yay for the arrow
					color: calculateColoursAndIcons().titleColor
				}}>
					{props.iconTitle}
				</span>
			</div>
			{generateConnector()}
		</div>
	);
}

export default NavigationIcon;