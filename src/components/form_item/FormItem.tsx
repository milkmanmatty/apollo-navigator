import React, { useState } from "react";
import iconColor from "../../types/iconColor";
import inputTemplate from "../../types/inputTemplate";
import './form-item.css'

type formItemProps = {
	borderColors: iconColor,
	template: inputTemplate,
	handleInputStatusChange: any
};

type formValue = string | number;

const FormItem = (props: formItemProps) => {

	const [formValue, setFormValue] = useState<formValue>("")
	const [formValueValid, setFormValueValid] = useState(!props.template.required);

	const handleInputChange = (
		e:React.ChangeEvent<HTMLInputElement>,
		required = false,
		minLength: number | null,
		maxLength: number | null
	) => {
		const { name, value } = e.target;

		if(name !== undefined && name !== null){
			setFormValue(value);

			//Here be dragons - witness some bad basic validation.
			//This *should* be handled by a validation system or one
			// of the many existing 3rd party libraries
			let valid = true;
			if(required && value === ""){
				valid = false;
			} else if(minLength && value.length < minLength){
				valid = false;
			} else if(maxLength && value.length > maxLength){
				valid = false;
			}

			setFormValueValid(valid);
			props.handleInputStatusChange(valid, value === "");
		}
	}

	const renderInputForState = (
		type: string,
		label: string,
		name: string,
		required = false,
		minLength = 0,
		maxLength = 256
	) => {
		return (
			<label style={{display: "block"}}>
				{label}{required && "*"}:<br />
				<input 
					name={name}
					onChange={
						(e) => handleInputChange(e, required, minLength, maxLength)
					}
					style={{
						borderColor: formValueValid ? 
							props.borderColors.available :
							props.borderColors.invalid
					}}
					type={type}
					value={formValue}
				/>
			</label>
		)
	}

	return (
		<div className="dynamic-input" data-testid="dynamic-input">
			{renderInputForState(
				props.template.type,
				props.template.label,
				props.template.name,
				props.template.required,
				props.template.minLength,
				props.template.maxLength
			)}
		</div>
	);
}

export default FormItem;