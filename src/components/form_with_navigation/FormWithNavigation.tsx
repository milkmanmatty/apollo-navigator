import React, { useEffect, useRef, useState } from "react";
import { WithChildren } from "../../App";
import { FormFourData, FormOneData, FormThreeData, FormTwoData } from "../../data/FormData";
import { Tooltips } from "../../data/IconTooltips";
import iconColor from "../../types/iconColor";
import iconPictures from "../../types/iconPictures";
import navStatus from "../../types/navStatus";
import NavigationIcon from "../navigation_icon/NavigationIcon";
import NavigationSection from "../navigation_section/NavigationSection";
import './form-with-nav.css';

//doesn't need to be extendible, will have children
type formWithNavProps = WithChildren<{
	navigationSections: number
}>;

const FormWithNavigation = (props: formWithNavProps) => {

	const [navSectionStatus, setNavSectionStatus] = useState<navStatus[]>([]);
	const [navSections, setNavSections] = useState<React.ReactNode[]>([]);
	const [curNavSec, setCurNavSec] = useState(0);

	//Ensure that updateNavSectionStatus function passed to NavSection children
	// always has up to date information since a new callback function is not
	// generated every render (which is the case normally).
	const statusRef = useRef([] as navStatus[]);
	statusRef.current = navSectionStatus;

	const sectionRef = useRef([] as React.ReactNode[]);
	sectionRef.current = navSections;

	const curActiveRef = useRef(0);
	curActiveRef.current = curNavSec;

	const formData = [FormOneData, FormTwoData, FormThreeData, FormFourData];

	useEffect(() => {
		setNavSections(
			initNavSections(
				props.navigationSections
			)
		);
		setNavSectionStatus(
			initNavSectionStatuses(
				props.navigationSections
			)
		);
	}, [props.navigationSections]);

	//Make these customizable by adding this to props.
	//In this instance just hardcode it as an example
	const hardCodedColors = ():iconColor => {
		return {
			available: "#d2ddef",
			unavailable: "#d2ddef",
			completed: "#384b5d",
			invalid: "#c05f68",
			active: "#6dc5ab"
		}
	}
	const hardCodedIcons = ():iconPictures => {
		//These are just css classes
		return {
			available: "icon-donut",
			unavailable: "small-dot",
			completed: "icon-check",
			invalid: "icon-exclamation"
		}
	}

	const updateNavSectionStatus = (index:number, newStatus:navStatus) => {
		setNavSectionStatus(
			statusRef.current.map((navStatus, i) => {
				if(i === index){
					return newStatus;
				}
				return navStatus;
			})
		);
	}

	const updateActiveNavSection = (newActive:number) => {
		if(newActive === curActiveRef.current){
			return;
		}
		if(navSectionStatus[newActive] !== "unavailable"){
			let replace = sectionRef.current!.map((ns, index) => {
				if(index === curActiveRef.current){
					return (
						<NavigationSection
							colors={hardCodedColors()}
							isAvailable={true}
							isActive={false}
							formItemTemplates={formData[index]}
							onComplete={progressNavSection.bind(this, index)}
							onStatusChange={updateNavSectionStatus.bind(this, index)}
							key={"nav_section_"+index}
						/>
					)
				} 
				if (index === newActive){
					return (
						<NavigationSection
							colors={hardCodedColors()}
							isAvailable={true}
							isActive={true}
							formItemTemplates={formData[index]}
							onComplete={progressNavSection.bind(this, index)}
							onStatusChange={updateNavSectionStatus.bind(this, index)}
							key={"nav_section_"+index}
						/>
					)
				}
				return ns;
			});
			setNavSections(replace);
			setCurNavSec(newActive);
		}
	}

	const progressNavSection = (completedSectionIndex: number) => {
		if(completedSectionIndex+1 === props.navigationSections){
			return;
		}
		updateNavSectionStatus(completedSectionIndex + 1, "available");
		updateActiveNavSection(completedSectionIndex + 1);
	}

	const initNavSections = (numNavSections: number) => {
		const result = Array(numNavSections);
		for(let i = 0; i < numNavSections; i++){
			result[i] = (
				<NavigationSection
					colors={hardCodedColors()}
					isAvailable={i === curActiveRef.current}
					isActive={i === curActiveRef.current}
					formItemTemplates={formData[i]}
					onComplete={progressNavSection.bind(this, i)}
					onStatusChange={updateNavSectionStatus.bind(this, i)}
					key={"nav_section_"+i}
				/>
			)
		}
		return result;
	}

	const initNavSectionStatuses = (numNavSections: number) => {
		let init:navStatus[] = [];
		for(let i = 0; i < numNavSections; i++){
			init[i] = !i ? "available" : "unavailable";
		}
		return init;
	}

	const generateNavIcons = (numNavSections: number) => {
		const colors:iconColor = {
			available: "#d2ddef",
			unavailable: "#d2ddef",
			completed: "#384b5d",
			invalid: "#c05f68",
			active: "#6dc5ab"
		}
		const result = Array(numNavSections);
		for(let i = 0; i < numNavSections; i++){
			result[i] = (
				<NavigationIcon
					colors={colors}
					icons={hardCodedIcons()}
					iconIndex={i}
					iconTitle={Tooltips[i]}
					isActive={curNavSec === i}
					key={"nav_icon_"+i}
					onIconClick={updateActiveNavSection}
					parentListCapacity={numNavSections}
					status={navSectionStatus[i]}
				/>
			)
		}
		return result;
	}

	const renderNavIcons = () => {
		return (
			<div className="nav-icons-list">
				{generateNavIcons(props.navigationSections)}
			</div>
		)
	}

	const renderNavSections = () => {
		return (
			<div className="nav-section-list">
				{navSections}
			</div>
		)
	}

	return (
		<div className="form-with-nav" data-testid="form-with-nav">
			{renderNavIcons()}
			{renderNavSections()}
		</div>
	);
}

export default FormWithNavigation;