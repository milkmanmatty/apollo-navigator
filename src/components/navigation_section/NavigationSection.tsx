import React, { useEffect, useRef, useState } from "react";
import iconColor from "../../types/iconColor";
import inputTemplate from "../../types/inputTemplate";
import navStatus from "../../types/navStatus";
import FormItem from "../form_item/FormItem";
import './navigation-section.css';

type navSectionProps = {
	colors: iconColor,
	isAvailable: boolean,
	isActive: boolean,
	formItemTemplates: inputTemplate[],
	onComplete: any,
	onStatusChange: any
};

const NavigationSection = (props: navSectionProps) => {

	const [status, setStatus] = useState((props.isActive ? "available" : "unavailable") as navStatus);
	const [formItems, setFormItems] = useState([] as JSX.Element[]);
	const [formItemsValid, setFormItemsValid] = useState(
		props.formItemTemplates.map((it) => {
			return !it.required
		})
	);
	const [formItemsEmpty, setFormItemsEmpty] = useState(
		props.formItemTemplates.map((it) => {
			return true
		})
	);

	//Ensure that callback function handleInputStatusChange, which is set in useEffect
	// below, always has up to date information since a new callback function is not
	// generated every render (which is the case normally).
	const validRef = useRef([] as boolean[]);
	validRef.current = formItemsValid;

	const emptyRef = useRef([] as boolean[]);
	emptyRef.current = formItemsEmpty;

	//On Init, load formItems. Can't be done in useState(default) because
	// handleInputStatusChange hasn't been initialized yet (thus ReferenceError).
	useEffect(() => {
		let formItemNodes = props.formItemTemplates.map((fit, index) => {
			return (
				<FormItem
					borderColors={props.colors}
					key={"form-item-"+index}
					handleInputStatusChange={
						handleInputStatusChange.bind(this, index)
					}
					template={fit}
				/>
			)
		});
		setFormItems(formItemNodes);
	}, [props.colors, props.formItemTemplates])

	useEffect(() => {
		setStatus(calculateSectionStatus());
	}, [formItemsValid, props.isActive])

	useEffect(() => {
		props.onStatusChange(status);
	}, [status, props]);

	const handleInputStatusChange = (
		formItemIndex: number,
		isValid: boolean,
		isEmpty: boolean
	) => {
		let update = validRef.current.map((fiv, index) => {
			if(index === formItemIndex){
				return isValid
			}
			return fiv
		});
		setFormItemsValid(update);

		let empties = emptyRef.current.map((fie, index) => {
			if(index === formItemIndex){
				return isEmpty
			}
			return fie;
		});
		setFormItemsEmpty(empties);
	}

	const calculateSectionStatus = ():navStatus => {
		if(!props.isAvailable) {
			return "unavailable";
		}
		if(areAllFormItemsEmpty() && !areAllFormItemsValid()){
			return "available";
		}
		if(!areAllFormItemsValid()){
			return "invalid";
		}
		if(areAllFormItemsValid()){
			return "completed";
		}
		return "available";
	}

	const areAllFormItemsValid = () => {
		for(let i = 0; i < formItemsValid.length; i++){
			if(!formItemsValid[i]){
				return false;
			}
		}
		return true;
	}

	const areAllFormItemsEmpty = () => {
		for(let i = 0; i < formItemsEmpty.length; i++){
			if(!formItemsEmpty[i]){
				return false;
			}
		}
		return true;
	}

	const getBorderColor = () => {
		if(status === "completed"){
			return props.colors.completed;
		} else if(status === "invalid") {
			return props.colors.invalid;
		}
		return props.colors.available;
	}

	const completeNavSection = () => {
		setStatus("completed");
		props.onComplete();
	}

	return (
		<div
			className="nav-section"
			data-testid="nav-section"
			style={{ 
				display: props.isActive ? "" : "none",
				borderColor: getBorderColor(),
			}}
		>
			{formItems}
			<div className="nav-section-footer">
				<button
					className="submit-button"
					disabled={!areAllFormItemsValid()}
					onClick={() => completeNavSection()}
					type="button"
				>
					Save And Continue
				</button>
			</div>
		</div>
	);
}

export default NavigationSection;