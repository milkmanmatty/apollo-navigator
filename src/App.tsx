import React from 'react';
import './App.css';
import FormWithNavigation from './components/form_with_navigation/FormWithNavigation';

export type WithChildren<T = {}> =
    T & { children?: React.ReactNode };

function App() {
	return (
		<div className="App" data-testid="app">
			<FormWithNavigation navigationSections={4} />
		</div>
	);
}

export default App;
