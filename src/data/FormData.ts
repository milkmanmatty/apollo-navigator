import inputTemplate from "../types/inputTemplate";

/*
This is placeholder data that would normally come from a database or some other data source.
*/

export const FormOneData:inputTemplate[] = [
	{
		type: "text",
		label: "A Required Input",
		name: "f1-input1",
		required: true,
		maxLength: 256,
		minLength: 0,
		validValues: []
	}
];

export const FormTwoData:inputTemplate[] = [
	{
		type: "text",
		label: "Form Two Data Input",
		name: "f2-input1",
		required: false,
		maxLength: 256,
		minLength: 0,
		validValues: []
	},
	{
		type: "text",
		label: "More than 10 Characters",
		name: "f2-input2",
		required: true,
		maxLength: 256,
		minLength: 10,
		validValues: []
	},
];

export const FormThreeData:inputTemplate[] = [
	{
		type: "text",
		label: "An exciting but useless input",
		name: "f3-input1",
		required: false,
		maxLength: 256,
		minLength: 0,
		validValues: []
	},
	{
		type: "number",
		label: "Favourite Number",
		name: "f3-input2",
		required: false,
		maxLength: 4,
		minLength: 0,
		validValues: []
	},
	{
		type: "text",
		label: "No more than 4 Characters",
		name: "f3-input3",
		required: true,
		maxLength: 4,
		minLength: 0,
		validValues: []
	},
];

export const FormFourData:inputTemplate[] = [
	{
		type: "text",
		label: "Please enter your bank password",
		name: "f4-input1",
		required: true,
		maxLength: 256,
		minLength: 0,
		validValues: []
	},
	{
		type: "number",
		label: "Amount of doggos seen today",
		name: "f4-input2",
		required: false,
		maxLength: 3,
		minLength: 0,
		validValues: []
	},
	{
		type: "text",
		label: "Please enter the Queen's phone number",
		name: "f4-input3",
		required: false,
		maxLength: 14,
		minLength: 0,
		validValues: []
	},
	{
		type: "text",
		label: "How many days until New Year's Eve?",
		name: "f4-input4",
		required: true,
		maxLength: 3,
		minLength: 0,
		validValues: []
	},
];