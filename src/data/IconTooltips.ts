/*
This is placeholder data that would normally come from a database or some other data source.
*/

export const Tooltips:string[] = [
	"Humble beginnings",
	"A slow but steady increase",
	"Monotony ensues",
	"Nonetheless, victory is imminent"
];