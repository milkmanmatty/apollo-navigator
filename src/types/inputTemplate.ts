type inputTemplate = {
	type: string,
	label: string,
	name: string,
	required: boolean,
	minLength: number,
	maxLength: number,
	validValues: string[]
}

export default inputTemplate