type navStatus =
	"available" |
	"unavailable" |
	"completed" |
	"invalid";

export default navStatus;