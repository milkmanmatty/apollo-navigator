type iconColor = {
	available: string,
	unavailable: string,
	completed: string,
	invalid: string
	active: string
}

export default iconColor;