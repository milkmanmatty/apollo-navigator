//This is effectively the same type as iconColor,
// but they may differ later on in development if features/requiirements change.
type iconPictures = {
	available: string,
	unavailable: string,
	completed: string,
	invalid: string
}

export default iconPictures